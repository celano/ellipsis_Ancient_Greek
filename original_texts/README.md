# Texts

This folder contains the treebank texts. More precisely, the texts comes from
two different repositories:

- The Ancient Greek Dependency Treebank 2.1
(https://github.com/PerseusDL/treebank_data/releases/tag/v2.1_IGDS)
- The Gorman Trees (https://github.com/vgorman1/Greek-Dependency-Trees). The
texts derive from the master branch downloaded in August 2022 (indeed, 
the available releases are older and contain fewer texts)

The texts have been merged in the `merge` folder (if a text was in both 
datasets, the one in the Gorman Trees has been chosen).

For consistency reasons, the file names of the Gorman Trees
have been changed into the format "tlgXXXX.tlgXXX."

