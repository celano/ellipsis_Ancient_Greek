import csv
import numpy as np
from sklearn.dummy import DummyClassifier
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import confusion_matrix

train_p = "../normalized_texts/train.csv"
val_p = "../normalized_texts/val.csv"
test_p = "../normalized_texts/val.csv"

with open(train_p) as f:
   reader = csv.DictReader(f)
   train_data = [row for row in reader]

x_train = np.array([v for x in train_data for k,v in x.items() 
                                                           if k == "sentence"])
y_train = [int(v) for x in train_data for k,v in x.items() 
                                                    if k == "elliptical_nodes"]
y_train = np.array(y_train)

with open(test_p) as f:
   reader = csv.DictReader(f)
   test_data = [row for row in reader]

x_test = np.array([v for x in test_data for k,v in x.items() if
                                                              k == "sentence"])
y_test = [int(v) for x in test_data for k,v in x.items() if
                                                       k == "elliptical_nodes"]
y_test = np.array(y_test)

dummy_clf = DummyClassifier(strategy="most_frequent")
dummy_clf.fit(x_train, y_train)
dummy = dummy_clf.predict(x_test)

print(precision_recall_fscore_support(y_test, dummy, average='micro',
                                                             labels=[0,1,2,3]))
print(precision_recall_fscore_support(y_test, dummy, average='macro',
                                                             labels=[0,1,2,3]))
print(precision_recall_fscore_support(y_test, dummy, average='weighted',
                                                             labels=[0,1,2,3]))
print(precision_recall_fscore_support(y_test, dummy, average=None,
                                                             labels=[0,1,2,3]))
print(confusion_matrix(y_test, dummy))


'''
(0.7706748664832497, 0.7706748664832497, 0.7706748664832497, None)
/home/technician/miniconda3/lib/python3.8/site-packages/sklearn/metrics/
_classification.py:1344: UndefinedMetricWarning: Precision and F-score 
are ill-defined and being set to 0.0 in labels with no predicted samples. 
Use `zero_division` parameter to control this behavior.
  _warn_prf(average, modifier, msg_start, len(result))
(0.19266871662081242, 0.25, 0.21762178959875697, None)

/home/technician/miniconda3/lib/python3.8/site-packages/sklearn/metrics/
_classification.py:1344: UndefinedMetricWarning: Precision and F-score are
ill-defined and being set to 0.0 in labels with no predicted samples.
Use `zero_division` parameter to control this behavior.
  _warn_prf(average, modifier, msg_start, len(result))

(0.5939397498289748, 0.7706748664832497, 0.6708625745714716, None)
/home/technician/miniconda3/lib/python3.8/site-packages/sklearn/metrics/
_classification.py:1344: UndefinedMetricWarning: 
Precision and F-score are ill-defined and being set to 0.0 in 
labels with no predicted samples. Use `zero_division` parameter to control 
this behavior.
  _warn_prf(average, modifier, msg_start, len(result))
(array([0.        , 0.        , 0.        , 0.77067487]), 
array([0., 0., 0., 1.]), 
array([0.        , 0.        , 0.        , 0.87048716]), 
array([1122,  206,   89, 4762]))
[[   0    0    0 1122]
 [   0    0    0  206]
 [   0    0    0   89]
 [   0    0    0 4762]]
'''
