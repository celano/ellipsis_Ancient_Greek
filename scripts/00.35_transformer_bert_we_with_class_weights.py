import csv
import numpy as np
import tensorflow as tf
import tensorflow_text as text
from tensorflow import keras
from tensorflow.keras import layers
import random
from sklearn.utils.class_weight import compute_class_weight
from transformers import AutoTokenizer, TFAutoModel
import matplotlib.pyplot as plt

train_p = "../normalized_texts/train.csv"
val_p = "../normalized_texts/val.csv"
test_p = "../normalized_texts/val.csv"

with open(train_p) as f:
   reader = csv.DictReader(f)
   train_data = [row for row in reader]

x_train = [v for x in train_data for k,v in x.items() 
                                                           if k == "sentence"]
y_train = [int(v) for x in train_data for k,v in x.items() 
                                                    if k == "elliptical_nodes"]
y_train = np.array(y_train)

with open(val_p) as f:
   reader = csv.DictReader(f)
   val_data = [row for row in reader]

x_val = [v for x in val_data for k,v in x.items() if
                                                              k == "sentence"]
y_val = [int(v) for x in val_data for k,v in x.items() if
                                                       k == "elliptical_nodes"]
y_val = np.array(y_val)

with open(test_p) as f:
   reader = csv.DictReader(f)
   test_data = [row for row in reader]

x_test = [v for x in test_data for k,v in x.items() if
                                                              k == "sentence"]
y_test = [int(v) for x in test_data for k,v in x.items() if
                                                       k == "elliptical_nodes"]
y_test = np.array(y_test)


all_labels = ["[0]", "[1]", "[2]", "none"]
dict_y = dict(zip(all_labels, range(0, len(all_labels))))
dict_y_inv = {v: k for k, v in dict_y.items()}

# https://huggingface.co/pranaydeeps/Ancient-Greek-BERT/blob/main/
# pytorch_model.bin
# 24 Sept 2021
tokenizer = AutoTokenizer.from_pretrained("pranaydeeps/Ancient-Greek-BERT")
model_pre = TFAutoModel.from_pretrained("pranaydeeps/Ancient-Greek-BERT", 
                                        from_pt=True)

# the following is necessary to know the longest sentence length in train:
# if a longer sentence is in valid or test, it is truncated
tokenized = tokenizer(x_train, return_tensors="tf", padding="longest")
max_length = tokenized["input_ids"].shape[1] # 364
print(max_length)
del tokenized

def create_dataset(x, y):
  tokenized = tokenizer(x, return_tensors="tf", max_length=max_length,
                                                padding="max_length")
  x = tf.data.Dataset.from_tensor_slices((tokenized["input_ids"],
                                          tokenized["attention_mask"]))
  y = tf.data.Dataset.from_tensor_slices(y)
  xy = tf.data.Dataset.zip((x,y))
  return xy.batch(8)

train = create_dataset(x_train, y_train)
val = create_dataset(x_val, y_val)
test = create_dataset(x_test, y_test)

##### MODEL

class TransformerBlock(layers.Layer):
    def __init__(self,embed_dim, num_heads, ff_dim, rate=0.2, **kwargs):
        super().__init__(**kwargs)
        self.att = layers.MultiHeadAttention(num_heads=num_heads,
                                             key_dim=embed_dim)
        self.ffn = keras.Sequential(
            [layers.Dense(ff_dim, activation="relu"), layers.Dense(embed_dim),]
        )
        self.layernorm1 = layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = layers.LayerNormalization(epsilon=1e-6)
        self.dropout1 = layers.Dropout(rate)
        self.dropout2 = layers.Dropout(rate)
        self.supports_masking = True

    def call(self, inputs, training, mask=None):
        mask_ = tf.cast(mask[:, tf.newaxis, :], dtype="int32")
        attn_output = self.att(inputs, inputs, attention_mask=mask_)
        attn_output = self.dropout1(attn_output, training=training)
        out1 = self.layernorm1(inputs + attn_output)
        ffn_output = self.ffn(out1)
        ffn_output = self.dropout2(ffn_output, training=training)
        return self.layernorm2(out1 + ffn_output)

class PositionEmbedding(layers.Layer):
    def __init__(self, maxlen, embed_dim):
        super().__init__()
        self.pos_emb = layers.Embedding(input_dim=maxlen,
                                        output_dim=embed_dim, mask_zero=True)

    def call(self, x):
        maxlen = tf.shape(x)[-1]
        positions = tf.range(start=0, limit=maxlen, delta=1)
        positions = self.pos_emb(positions)
        positions = positions[tf.newaxis, :, :]
        #positions = tf.repeat(positions, repeats=[batch_size], axis=0)
        return positions

vocab_size = tokenizer.vocab_size
embed_dim = 768
num_heads = 2
ff_dim = 768

inputs = layers.Input(shape=(max_length,), dtype=tf.int32)
masks = layers.Input(shape=(max_length,), dtype=tf.int32)
x = model_pre.bert(inputs, attention_mask=masks)["last_hidden_state"]
position_emb = PositionEmbedding(max_length, embed_dim)
y = position_emb(inputs)
x = layers.Add()([x, y])
transformer_block = TransformerBlock(embed_dim, num_heads, ff_dim)
x = transformer_block(x, mask=masks)
x = layers.GlobalAveragePooling1D()(x, mask=masks)
x = layers.Dropout(0.2)(x)
x = layers.Dense(500, activation="relu")(x)
x = layers.Dense(100, activation="relu")(x)
x = layers.Dropout(0.2)(x)
outputs = layers.Dense(len(dict_y), activation="softmax")(x)

model = keras.Model(inputs=[inputs, masks], outputs=outputs)
model.summary()

early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss',
                                                  patience=2,
                                                  restore_best_weights=True)



class_weights = compute_class_weight(class_weight = "balanced",
                 classes= np.unique(y_train), y= y_train)
class_weights = dict(zip(range(0,len(dict_y)), class_weights))

opt = tf.keras.optimizers.Adam(learning_rate=0.000001)
model.compile(optimizer=opt, loss="sparse_categorical_crossentropy",
              metrics=["sparse_categorical_accuracy"])
history = model.fit(train, epochs=10, validation_data=[val]
,
class_weight=class_weights,
callbacks=[early_stopping])

##### EVALUATE

model.evaluate(test)
#[0.5244752764701843, 0.7871823906898499]
pre = model.predict(test)
pre = np.argmax(pre, axis=-1)
# np.savetxt("_00.35_transformer_bert_we_with_class_weights.csv", pre, fmt="%d", delimiter=",")
from sklearn.metrics import precision_recall_fscore_support
print(precision_recall_fscore_support(y_test, pre, average='micro', labels=[0,1,2,3]))
#(0.7871823919728111, 0.7871823919728111, 0.7871823919728111, None)
print(precision_recall_fscore_support(y_test, pre, average='macro', labels=[0,1,2,3]))
#(0.5379332018938658, 0.5346395103871391, 0.5116387248066951, None)
print(precision_recall_fscore_support(y_test, pre, average='weighted', labels=[0,1,2,3]))
#(0.8086574150789256, 0.7871823919728111, 0.7945148297799485, None)
precision_recall_fscore_support(y_test, pre, average=None, labels=[0,1,2,3])
#(array([0.52197213, 0.2027027 , 0.51923077, 0.9078272 ]),
# array([0.43404635, 0.50970874, 0.30337079, 0.89143217]),
# array([0.47396594, 0.29005525, 0.38297872, 0.89955499]),
# array([1122,  206,   89, 4762]))

from sklearn.metrics import confusion_matrix
confusion_matrix(y_test, pre)
#array([[ 487,  231,    9,  395],
#       [  59,  105,   12,   30],
#       [   7,   49,   27,    6],
#       [ 380,  133,    4, 4245]])
