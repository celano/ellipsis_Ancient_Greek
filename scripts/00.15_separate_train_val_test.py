import json
import csv
import numpy as np
from sklearn.model_selection import train_test_split
import random

##### IMPORT DATASET

path = "../normalized_texts/sentences_for_machine_learning.json"
with open(path, 'r') as f:
    data = json.load(f)

data = [dict(x, target=x["target"].split()) for x in data]
all_sentences = [x["sentence"] for x in data]

values_ellipsis = [x["target"][-1] for x in data]
all_labels = set(values_ellipsis)
dict_plot = {x: values_ellipsis.count(x) for x in all_labels}
dict_plot2 = {int(k.replace("[", "").replace("]", "")) + 1:v for k,v
in dict_plot.items() if k != "none"}
dict_plot2 = sorted(dict_plot2.items())

# it turns out that it is reasonable to consider
# only the following numbers of elliptical nodes,
# as there are only few examples of the others
all_labels = ["[0]", "[1]", "[2]", "none"]

dict_y = dict(zip(all_labels, range(0, len(all_labels))))
dict_y_inv = {v: k for k, v in dict_y.items()}

matrix_y = np.zeros((len(data)), dtype=np.int32)
for nmb1, x in enumerate(data):
  for k,v in x.items():
    if k == "target":
       nmb2 = v[-1]
       if nmb2 not in dict_y.keys():
          nmb2 = "[2]"
       matrix_y[nmb1] = dict_y[nmb2]

# split into 80%, 10%, 10%
x_train, x_test, y_train, y_test = train_test_split(all_sentences, matrix_y,
                        test_size=0.1, stratify=matrix_y, random_state=42)

x_train, x_val, y_train, y_val = train_test_split(x_train, y_train,
                        test_size=0.125, stratify=y_train, random_state=42)

indices = list(range(0, len(x_train)))
random.shuffle(indices)
x_train = np.array(x_train)[indices].tolist()
y_train = y_train[indices]

def create_csv(x_set, y_set, type_):
  fields=["sentence", "elliptical_nodes"]
  rows = [[x, y] for x,y in zip(x_set, y_set)]
  with open('../normalized_texts/' + type_ + '.csv', 'w') as f:
    write = csv.writer(f)
    write.writerow(fields)
    write.writerows(rows)

create_csv(x_train, y_train, 'train')
create_csv(x_val, y_val, 'val')
create_csv(x_test, y_test, 'test')
