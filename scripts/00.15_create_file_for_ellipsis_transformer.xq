(: Basex 10.5 :)

declare option output:indent 'yes';

declare variable $p := 
doc("../normalized_texts/all_sentences_final.xml");

(: 
error in the treebank: some apheresis is treated as a separate token 
and encoded as &#x0313; or rarely as &#x1fbd;
:)

declare function local:normalize($collection)
{
      for $s in $p//sentence
      return
      <s id="{$s/@id}" doc_id="{$s/@doc_id}">
      {
       for $t in $s/word
       return
       if (not($t/@artificial)) then
       <t ell="no">
       {
       $t/@* 
       }
       </t>
       else
       <t ell="yes">
       {
       $t/@*
       }
       </t>
       }</s>
};

declare function local:punctuation($sentences)
{
      for $s in $sentences
      return
      <s id="{$s/@id}" doc_id="{$s/@doc_id}">
      {
       for $t in $s/t
       return
       if (matches($t/@form, "\p{P}") and string-length($t/@form) = 1) then
       <t punct="yes">{$t/@*}</t>
       else
       <t punct="no">{$t/@*}</t>
      }
      </s>
};

declare function local:group-ellipsis($sentences)
{
     for $s in $sentences
     return
     <s id="{$s/@id}" doc_id="{$s/@doc_id}">
     {
      for $t in $s/t
      group by $g1 :=$t/@ell
      return
      <g1 ell="{$g1}" multi="{string-join($t/@form, " ")}">
      {$t}
      </g1>
     }
     </s>
};

(: this is to replace cases of [3] [2] [1] into [0] [1] [2] :)
declare function local:rename-form-ellipsis($sentences)
{
 for $s in $sentences
 let $g1n := $s/g1[@ell="no"]
 let $g1y := for $t at $count in $s/g1[@ell="yes"]//t
             return 
             element t {$t/@* except ($t/@form, $t/@lemma),
                       attribute form  {"["|| $count - 1 ||"]"},
                       attribute lemma {"["|| $count - 1 ||"]"},
                       attribute original_form {$t/@form}} 
 return
 if ($s/g1[@ell="yes"])
 then
 element s {$s/@*,
 ($s/g1[@ell="no"],
 <g1 ell="yes" multi="{string-join(for $t in $g1y return $t/@form, " ")}">
 {$g1y}
 </g1>
 )
 }
 else 
 $s
};

declare function local:merge-ellipsis($p) 
{
     for $s in local:normalize($p)=>local:punctuation()=>local:group-ellipsis()
     => local:rename-form-ellipsis()
     return
      <s id="{$s/@id}" doc_id="{$s/@doc_id}">{
      if ($s/g1[@ell="yes"])
      then
      (
      for $t in $s/g1[@ell="no"]/t[position()!= last()]
      return element t {$t/@*, attribute predict {""}}
      
      ,
      let $last := $s//g1[@ell="no"]/t[position()= last()]
      return
      element t {attribute form {$last/@form}, attribute id {$last/@id},
                 attribute predict {$s/g1[@ell="yes"]/@multi}, 
                 $last/@punct })
      else
      for $t in $s/g1[@ell="no"]/t
      return
      (element t {$t/@*, attribute predict {""}})

      }</s>
};

array {
for $s at $n_sent in local:merge-ellipsis($p)
let $t := $s/t
let $join := string-join($t/@form, " ")
let $predict := if ($t[last()]/@predict != "") 
                then data($t[last()]/@predict)                 
                else "none"

return
 map {
'id' : $n_sent,
'doc' :  data($s/@doc_id),
'sent_id' : data($s/@id),
'sentence' : $join,
'target' : $predict
     }
}

=>
serialize(
  map {
    'method': 'json',
    'indent': "yes"
      }
         )
  
                                