import numpy as np
import tensorflow as tf
import tensorflow_text as text
from tensorflow import keras
from tensorflow.keras import layers
from transformers import AutoTokenizer, TFAutoModel
import argparse

def predict(all_sentences):
  tokenized = tokenizer(all_sentences,return_tensors="tf",
                                  max_length=364, padding="max_length",
                                  truncation=True)
  x1, x2 = (tokenized["input_ids"], tokenized["attention_mask"])

  pre = model.predict((x1, x2), batch_size=8)
  pre = np.argmax(pre, axis=-1)
  file = open(args.output_path, "w")
  o_s = []
  for s, x in zip(all_sentences, pre):
    if x == 3:
       o_s.append(s + " [4]")
    elif x == 0:
       o_s.append(s + " [1]")
    elif x == 1:
       o_s.append(s + " [2]")
    elif x == 2:
       o_s.append(s + " [3]")
  print(*o_s, sep="\n", file=file)

if __name__ == "__main__":
   parser = argparse.ArgumentParser(
                    prog='Elleipsis',
                    description='An ellipsis annotator for Ancient Greek',
                    epilog='')
   parser.add_argument('input_file',
   help="this is the file containing sentences already tokenized")
   parser.add_argument('output_path',
   help="this is the path where to save the result")
   args = parser.parse_args()
   file_name = args.input_file
   with open(file_name) as f:
        t = f.read()
        t = t.splitlines()
   tokenizer = AutoTokenizer.from_pretrained("pranaydeeps/Ancient-Greek-BERT")
   model_pre = TFAutoModel.from_pretrained("pranaydeeps/Ancient-Greek-BERT",
                                                                 from_pt=True)
   
   model = keras.models.load_model('../models/feedforward_bert_we/mb')
   predict(t)
