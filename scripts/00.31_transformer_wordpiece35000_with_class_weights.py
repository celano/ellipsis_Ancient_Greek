import csv
import numpy as np
import tensorflow as tf
import tensorflow_text as text
from tensorflow import keras
from tensorflow.keras import layers
import random
from sklearn.utils.class_weight import compute_class_weight
from transformers import AutoTokenizer, TFAutoModel
import matplotlib.pyplot as plt

# https://www.tensorflow.org/text/guide/subwords_tokenizer
# https://keras.io/examples/nlp/text_classification_with_transformer/
# https://www.tensorflow.org/text/tutorials/transformer#multi-head_attention
# https://huggingface.co/bert-base-multilingual-cased

#np.random.seed(123)
#random.seed(123)
#tf.random.set_seed(1234)

train_p = "../normalized_texts/train.csv"
val_p = "../normalized_texts/val.csv"
test_p = "../normalized_texts/val.csv"

with open(train_p) as f:
   reader = csv.DictReader(f)
   train_data = [row for row in reader]

x_train = [v for x in train_data for k,v in x.items() 
                                                           if k == "sentence"]
y_train = [int(v) for x in train_data for k,v in x.items() 
                                                    if k == "elliptical_nodes"]
y_train = np.array(y_train)

with open(val_p) as f:
   reader = csv.DictReader(f)
   val_data = [row for row in reader]

x_val = [v for x in val_data for k,v in x.items() if
                                                              k == "sentence"]
y_val = [int(v) for x in val_data for k,v in x.items() if
                                                       k == "elliptical_nodes"]
y_val = np.array(y_val)

with open(test_p) as f:
   reader = csv.DictReader(f)
   test_data = [row for row in reader]

x_test = [v for x in test_data for k,v in x.items() if
                                                              k == "sentence"]
y_test = [int(v) for x in test_data for k,v in x.items() if
                                                       k == "elliptical_nodes"]
y_test = np.array(y_test)

# it turns out that it is reasonable to consider only the following
# number of elliptical nodes, the others being just a few examples
all_labels = ["[0]", "[1]", "[2]", "none"]

dict_y = dict(zip(all_labels, range(0, len(all_labels))))
dict_y_inv = {v: k for k, v in dict_y.items()}

bert_tokenizer_params=dict(lower_case=True)

gr_tokenizer = text.BertTokenizer('../normalized_texts/greek_vocab_35000.txt', 
                                  **bert_tokenizer_params)

matrix_x = gr_tokenizer.tokenize(x_train)
matrix_x = matrix_x.merge_dims(-2,-1)
lm = max([len(x) for x in matrix_x.to_list()])

with open('../normalized_texts/greek_vocab_35000.txt', 'r') as f:
    greek_vocab = f.read().splitlines()

def prepare_dataset(x, y, lm):
   tokenized = gr_tokenizer.tokenize(x)
   tokenized = tokenized.merge_dims(-2,-1)
   matrix_x = tf.keras.utils.pad_sequences(tokenized.numpy(), 
                     padding="post", maxlen=lm)
   return matrix_x, y

x_train, y_train = prepare_dataset(x_train, y_train, lm)
x_val, y_val = prepare_dataset(x_val, y_val, lm)
x_test, y_test = prepare_dataset(x_test, y_test, lm)

# model
class TransformerBlock(layers.Layer):
    def __init__(self,embed_dim, num_heads, ff_dim, rate=0.2,**kwargs):
        super().__init__(**kwargs)
        self.att = layers.MultiHeadAttention(num_heads=num_heads,
                                             key_dim=embed_dim)
        self.ffn = keras.Sequential([layers.Dense(ff_dim, activation="relu"), 
                                     layers.Dense(embed_dim),])
        self.layernorm1 = layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = layers.LayerNormalization(epsilon=1e-6)
        self.dropout1 = layers.Dropout(rate)
        self.dropout2 = layers.Dropout(rate)
        self.supports_masking = True

    def call(self, inputs, training, mask=None):
        #if mask is not None:
        mask_ = tf.cast(mask[:, tf.newaxis, :], dtype="int32")
        attn_output = self.att(inputs, inputs, attention_mask=mask_)
        attn_output = self.dropout1(attn_output, training=training)
        out1 = self.layernorm1(inputs + attn_output)
        ffn_output = self.ffn(out1)
        ffn_output = self.dropout2(ffn_output, training=training)
        return self.layernorm2(out1 + ffn_output), mask

class TokenAndPositionEmbedding(layers.Layer):
    def __init__(self, maxlen, vocab_size, embed_dim):
        super().__init__()
        self.token_emb = layers.Embedding(input_dim=vocab_size,
                                          output_dim=embed_dim, mask_zero=True)
        self.pos_emb = layers.Embedding(input_dim=maxlen,
                                        output_dim=embed_dim, mask_zero=True)

    def call(self, x):
        mask = self.token_emb.compute_mask(x)
        maxlen = tf.shape(x)[-1]
        positions = tf.range(start=0, limit=maxlen, delta=1)
        positions = self.pos_emb(positions)
        x = self.token_emb(x)
        return x + positions, mask

maxlen = lm
vocab_size = len(greek_vocab)
embed_dim = 2000  # Embedding size for each token
num_heads = 2  # Number of attention heads
ff_dim = 2000 # Hidden layer size in feed forward network inside transformer



inputs = layers.Input(shape=x_train.shape[1])
embedding_layer = TokenAndPositionEmbedding(maxlen, vocab_size, embed_dim)
x = embedding_layer(inputs)
transformer_block = TransformerBlock(embed_dim, num_heads, ff_dim)
x = transformer_block(x[0], mask=x[1])
x = layers.GlobalAveragePooling1D()(x[0], mask=x[1])
x = layers.Dropout(0.2)(x)
x = layers.Dense(500, activation="relu")(x)
x = layers.Dense(100, activation="relu")(x)
x = layers.Dropout(0.2)(x)
outputs = layers.Dense(len(dict_y), activation="softmax")(x)

model = keras.Model(inputs=inputs, outputs=outputs)
model.summary()
early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss',
                                                  patience=2,
                                                  restore_best_weights=True)

class_weights = compute_class_weight(class_weight = "balanced",
                 classes= np.unique(y_train), y= y_train)
class_weights = dict(zip(range(0,len(dict_y)), class_weights))

opt = tf.keras.optimizers.Adam(learning_rate=0.000001)
model.compile(optimizer=opt, loss="sparse_categorical_crossentropy",
              metrics=["sparse_categorical_accuracy"])
history = model.fit(
    x_train, y_train, epochs=30, batch_size=8, validation_data=[x_val, y_val], 
callbacks=[early_stopping]
,class_weight=class_weights)
)

model.evaluate(np.array(x_test), y_test)
#[1.1720954179763794, 0.5403786897659302]
pre = model.predict(np.array(x_test))
pre = np.argmax(pre, axis=-1)

# np.save('../models/transformer_wordpiece35000_with_class_weights/predicted_test.npy', pre)
# pre= np.load('../models/transformer_wordpiece35000_with_class_weights/predicted_test.npy',
#allow_pickle=True)

from sklearn.metrics import precision_recall_fscore_support
print(precision_recall_fscore_support(y_test, pre, average='micro', labels=[0,1,2,3]))
print(precision_recall_fscore_support(y_test, pre, average='macro', labels=[0,1,2,3]))
print(precision_recall_fscore_support(y_test, pre, average='weighted', labels=[0,1,2,3]))
print(precision_recall_fscore_support(y_test, pre, average=None, labels=[0,1,2,3]))
from sklearn.metrics import confusion_matrix
print(confusion_matrix(y_test, pre))

#(0.5403787020553488, 0.5403787020553488, 0.5403787020553488, None)
#(0.3095328933896807, 0.39072880853836056, 0.2594988847588869, None)
#(0.6946337383827316, 0.5403787020553488, 0.5921403891470126, None)
#(array([0.31021898, 0.04153355, 0.06107784, 0.8253012 ]), array([0.07575758, 0.25242718, 0.57303371, 0.66169677]), array([0.1217765 , 0.07133059, 0.11038961, 0.73449883]), array([1122,  206,   89, 4762]))
#[[  85  269  192  576]
# [   6   52   77   71]
# [   1   17   51   20]
# [ 182  914  515 3151]]


# np.save('../models/transformer_wordpiece35000_with_class_weights/my_history.npy',history.history)
# history=np.load('../models/transformer_wordpiece35000_with_class_weights/my_history.npy',allow_pickle='TRUE')

'''
5407/5407 [==============================] - 475s 87ms/step - loss: 1.3161 - sparse_categorical_accuracy: 0.3519 - val_loss: 1.1721 - val_sparse_categorical_accuracy: 0.5404
Epoch 2/30
5407/5407 [==============================] - 429s 79ms/step - loss: 1.2567 - sparse_categorical_accuracy: 0.4467 - val_loss: 1.1980 - val_sparse_categorical_accuracy: 0.4968
Epoch 3/30
5407/5407 [==============================] - 428s 79ms/step - loss: 1.2256 - sparse_categorical_accuracy: 0.4644 - val_loss: 1.2214 - val_sparse_categorical_accuracy: 0.4695

'''
