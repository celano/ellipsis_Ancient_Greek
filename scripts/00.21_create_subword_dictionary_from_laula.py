import json
import glob
import lxml.etree
import tensorflow as tf
from tensorflow_text.tools.wordpiece_vocab import bert_vocab_from_dataset as bert_vocab

path = "../../ancientgreeknlp_local/workspace/oga/laula"

all_sentences = []
for name in glob.glob(path + "/*/*-seg01.xml"):
    file = lxml.etree.parse(name)
    mark = file.xpath("//m")
    for t in mark:
        sentence_text = t.attrib["r"]
        all_sentences.append(sentence_text)

dataset = tf.data.Dataset.from_tensor_slices(all_sentences)

bert_tokenizer_params=dict(lower_case=True)
reserved_tokens=["[PAD]", "[UNK]", "[START]", "[END]"]

bert_vocab_args = dict(
    vocab_size = 35000,
    reserved_tokens=reserved_tokens,
    bert_tokenizer_params=bert_tokenizer_params,
    learn_params={},
)

gr_vocab = bert_vocab.bert_vocab_from_dataset(
    dataset.batch(1000).prefetch(2),
    **bert_vocab_args
)

def write_vocab_file(filepath, vocab):
  with open(filepath, 'w') as f:
    for token in vocab:
      print(token, file=f)

write_vocab_file('../normalized_texts/greek_vocab_35000.txt', gr_vocab)

