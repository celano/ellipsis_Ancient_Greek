import csv
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
import matplotlib.pyplot as plt

train_p = "../normalized_texts/train.csv"
val_p = "../normalized_texts/val.csv"
test_p = "../normalized_texts/val.csv"

with open(train_p) as f:
   reader = csv.DictReader(f)
   train_data = [row for row in reader]

x_train = np.array([v for x in train_data for k,v in x.items() 
                                                           if k == "sentence"])
y_train = [int(v) for x in train_data for k,v in x.items() 
                                                    if k == "elliptical_nodes"]
y_train = np.array(y_train)

with open(val_p) as f:
   reader = csv.DictReader(f)
   val_data = [row for row in reader]

x_val = np.array([v for x in val_data for k,v in x.items() if
                                                              k == "sentence"])
y_val = [int(v) for x in val_data for k,v in x.items() if
                                                       k == "elliptical_nodes"]
y_val = np.array(y_val)

with open(test_p) as f:
   reader = csv.DictReader(f)
   test_data = [row for row in reader]

x_test = np.array([v for x in test_data for k,v in x.items() if
                                                              k == "sentence"])
y_test = [int(v) for x in test_data for k,v in x.items() if
                                                       k == "elliptical_nodes"]
y_test = np.array(y_test)

max_tokens=35000

vectorizer = tf.keras.layers.experimental.preprocessing.TextVectorization(
    max_tokens=max_tokens,
    standardize="lower_and_strip_punctuation",
    split="whitespace",
    ngrams=None,
    output_mode="tf-idf",
    output_sequence_length=None,
    pad_to_max_tokens=False,
    vocabulary=None,
)

text_dataset = tf.data.Dataset.from_tensor_slices(x_train)
vectorizer.adapt(text_dataset.batch(64))

# the labels are the ones identified when the datasets where created
all_labels = ["[0]", "[1]", "[2]", "none"]

dict_y = dict(zip(all_labels, range(0, len(all_labels))))
dict_y_inv = {v: k for k, v in dict_y.items()}

##### MODEL

inputs = layers.Input(shape=(), dtype="string")
x = vectorizer(inputs)
x = layers.Dense(5000, activation="relu") (x)
x = layers.Dense(2000, activation="relu") (x)
x = layers.Dense(1000, activation="relu") (x)
x = layers.Dropout(0.2)(x)
outputs = layers.Dense(len(dict_y), activation="softmax")(x)
model = keras.Model(inputs, outputs)
model.summary()
opt = tf.keras.optimizers.Adam(learning_rate=0.000001)
model.compile(optimizer=opt, loss="sparse_categorical_crossentropy",
              metrics=["sparse_categorical_accuracy"])
early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss',
                                                  patience=2,
                                                  restore_best_weights=True)

history = model.fit(x_train,y_train, epochs=30, 
batch_size=16, validation_data=[x_val, y_val]
,callbacks=[early_stopping])

#np.save('../models/baseline/my_history.npy',history.history)
# hist = np.load('../models/baseline/my_history.npy',
# allow_pickle=True)

##### EVALUATE

model.evaluate(x_test, y_test)
#[0.6704245209693909, 0.7719695568084717]
pre = model.predict(x_test)
pre = np.argmax(pre, axis=-1)

# np.save('../models/baseline/predicted_test.npy', pre)
# pre= np.load('../models/baseline/predicted_test.npy',
#allow_pickle=True)

print(precision_recall_fscore_support(y_test, pre, average='micro',
                                                             labels=[0,1,2,3]))
# (0.771969574364784, 0.771969574364784, 0.771969574364784, None)
print(precision_recall_fscore_support(y_test, pre, average='macro',
                                                             labels=[0,1,2,3]))
# (0.41894306009362287, 0.2740629881862671, 0.26622398527267455, None)
print(precision_recall_fscore_support(y_test, pre, average='weighted',
                                                             labels=[0,1,2,3]))
# (0.698714811582307, 0.771969574364784, 0.7077306177586686, None)
print(precision_recall_fscore_support(y_test, pre, average=None,
                                                             labels=[0,1,2,3]))
#(array([0.38040346, 0.5       , 0.        , 0.79536878]),
# array([0.11764706, 0.00485437, 0.        , 0.97375052]),
# array([0.17971409, 0.00961538, 0.        , 0.87556647]),
# array([1122,  206,   89, 4762]))
conf_ = confusion_matrix(y_test, pre)
print(pre)
#array([[ 132,    0,    0,  990],
#       [  52,    1,    0,  153],
#       [  38,    1,    0,   50],
#       [ 125,    0,    0, 4637]])
disp = ConfusionMatrixDisplay(confusion_matrix=conf_,
                               display_labels=all_labels)
disp.plot()
plt.show()
