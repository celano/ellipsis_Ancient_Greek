import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


conf_base_no_w = np.array([[ 132,    0,    0,  990],
       [  52,    1,    0,  153],
       [  38,    1,    0,   50],
       [ 125,    0,    0, 4637]])

sums = conf_base_no_w.sum(axis=1, keepdims=True)
conf_base_no_w = conf_base_no_w / sums
conf_base_no_w = np.round(conf_base_no_w, decimals=2)

conf_base_w = np.array([[ 414, 77,26,605],
 [  79,  21,   11,  95],
 [  34,  12,  10,  33],
 [ 893,154, 29, 3686]])

sums = conf_base_w.sum(axis=1, keepdims=True)
conf_base_w = conf_base_w / sums
conf_base_w = np.round(conf_base_w, decimals=2)

only_bert = np.array([[ 630,   29,    5,  458],
       [  93,   49,   14,   50],
       [  33,   25,   22,    9],
       [ 191,    1,    1, 4569]])

sums = only_bert.sum(axis=1, keepdims=True)
only_bert = only_bert / sums
only_bert = np.round(only_bert, decimals=2)

tr_35000 = np.array([[ 142,    0,    0,  980],
       [  33,    0,    0,  173],
       [  23,    0,    3,   63],
       [  94,    0,    3, 4665]])

sums = tr_35000.sum(axis=1, keepdims=True)
tr_35000 = tr_35000 / sums
tr_35000 = np.round(tr_35000, decimals=2)

lstm = np.array([[ 599,   29,   10,  484],
       [  97,   42,   15,   52],
       [  30,   24,   22,   13],
       [ 190,    2,    1, 4569]])

sums = lstm.sum(axis=1, keepdims=True)
lstm = lstm / sums
lstm = np.round(lstm, decimals=2)

tr_bert = np.array([[ 623,   40,   10,  449],
       [ 105,   41,   17,   43],
       [  32,   19,   27,   11],
       [ 217,    2,    2, 4541]])


sums = tr_bert.sum(axis=1, keepdims=True)
tr_bert = tr_bert / sums
tr_bert = np.round(tr_bert, decimals=2)


fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
ax1.set_xlabel("true")
ax2.set(xlabel="true")
ax2.set(ylabel="predicted")

sns.heatmap(
     data=conf_base_no_w,
     fmt="g",
     annot=True,
     cmap="Blues",
     xticklabels=[1, 2, ">2", "none"],
     yticklabels=[1, 2, ">2", "none"],
     ax=ax1
 )
sns.heatmap(
     data=conf_base_w,
     fmt="g",
     annot=True,
     cmap="Blues",
     xticklabels=[1, 2, ">2", "none"],
     yticklabels=[1, 2, ">2", "none"],
     ax=ax2
 )
ax1.tick_params(top=True, labeltop=True, bottom=False, labelbottom=False)
ax1.set_ylabel("true")
ax1.set_title("baseline (no class weights)")#, fontweight="bold")
ax2.set(ylabel="true")
ax2.set(xlabel="predicted")
ax2.set_title("baseline (with class weights)")

plt.show()


fig, (ax1, ax2) = plt.subplots(2, 2, sharex=True)
ax1[0].tick_params(top=True, labeltop=True, bottom=False, labelbottom=False)
ax1[1].tick_params(top=True, labeltop=True, bottom=False, labelbottom=False)


sns.heatmap(
     data=tr_35000,
     fmt="g",
     annot=True,
     cmap="Blues",
     xticklabels=[1, 2, ">2", "none"],
     yticklabels=[1, 2, ">2", "none"],
     ax=ax1[0], cbar=False
 )
sns.heatmap(
     data=only_bert,
     fmt="g",
     annot=True,
     cmap="Blues",
     xticklabels=[1, 2, ">2", "none"],
     yticklabels=[1, 2, ">2", "none"],
     ax=ax1[1],
cbar=True,
 )

sns.heatmap(
     data=tr_bert,
     fmt="g",
     annot=True,
     cmap="Blues",
     xticklabels=[1, 2, ">2", "none"],
     yticklabels=[1, 2, ">2", "none"],
     ax=ax2[0], cbar=False
 )
sns.heatmap(
     data=lstm,
     fmt="g",
     annot=True,
     cmap="Blues",
     xticklabels=[1, 2, ">2", "none"],
     yticklabels=[1, 2, ">2", "none"],
     ax=ax2[1]
 )

ax1[0].set_ylabel("true")
#ax1.yaxis.set_label_coords(1, 1)

#ax1[1].set_xlabel("true")
ax2[0].set_xlabel("predicted")
ax2[0].set(ylabel="true")
ax2[1].set_xlabel("predicted")
ax1[0].set_title("Transformer-WordPiece35000")
ax1[1].set_title("Feedforward-DBBE-BERT")#
ax2[0].set_title("Transformer-DBBE-BERT")#
ax2[1].set_title("LSTM-DBBE-BERT")#

plt.show()

