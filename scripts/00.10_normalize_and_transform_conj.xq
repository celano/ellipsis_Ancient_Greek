(: Basex 10.5 :)

declare option output:indent 'yes';

declare function local:add-artificial($collection)
{
  <treebank>{
  for $s in $collection//sentence
  return
  <sentence id="{$s/@id}" doc_id="{$s/@document_id}">
      {
       for $t in $s/word
       return
       if (matches($t/@form, "\[\p{N}+\]") and not($t/@artificial)) then
       <word>
       {
       $t/@*,
       attribute artificial {}
       }
       </word>
       else
       <word>
       {
       $t/@*
       }
       </word>
      }
   </sentence>
      }</treebank>
};

(: This query normalizes the Unicode and the tokenization scheme 
of the treebank :)

declare function local:apostrophe($string)
{
let $s :=  
$string 
=> replace("-", "") (: "-" to mean crasis is deleted :)
=> replace("&#x1fbd;", "&#x02bc;") (: GREEK KORONIS changed 
                                      into MODIFIER LETTER APOSTROPHE
                                      7224 cases
                                     :)

let $s3 :=
$s => replace("&#x0313;", "&#x02bc;" ) (: COMBINING COMMA ABOVE 
                                       changed into MODIFIER LETTER APOSTROPHE:
                                       28581 cases
                                       :)
let $s4 :=
$s3 => replace("'", "&#x02bc;" ) (:    APOSTROPHE
                                       changed into MODIFIER LETTER APOSTROPHE:
                                       11 cases
                                       :)

let $s5 :=
$s4 => replace("&#x2019;", "&#x02bc;") (: RIGHT SINGLE QUOTATION MARK changed 
                                         into MODIFIER LETTER APOSTROPHE
                                         4481   
                                       :)
(: MODIFIER LETTER APOSTROPHE 5269 cases :)
return
if (string-length($string) > 1) (: i.e., it is not a quation mark :)
then 
$s5

else
$s3                                  

}; 

declare function local:normalize($collection)
{
      for $s in $collection//sentence
      return
      <sentence id="{$s/@id}" doc_id="{$s/@doc_id}">
      {
       for $t in $s/word
       return
       if (not($t/@artificial)) then
       <word>
       {
      attribute id {normalize-unicode($t/@id, "NFC") },
      attribute form {normalize-unicode($t/@form, "NFC")=>local:apostrophe()},
      attribute lemma {normalize-unicode($t/@lemma, "NFC")=>local:apostrophe()},
      attribute postag {normalize-unicode($t/@postag, "NFC")},
      attribute relation {normalize-unicode($t/@relation, "NFC")},
      attribute head {normalize-unicode($t/@head, "NFC")}  
       }
       </word>
       else
       <word>
       {
      attribute id {normalize-unicode($t/@id, "NFC")},
      attribute form {normalize-unicode($t/@form, "NFC" )=>local:apostrophe()},
      attribute lemma {normalize-unicode($t/@form, "NFC" )=>local:apostrophe()},
      attribute postag {"---------"},
      attribute relation {normalize-unicode($t/@relation, "NFC")},
      attribute head {normalize-unicode($t/@head, "NFC")},
      attribute artificial {}
       }
       </word>
       }</sentence>
};

declare function local:modify-coord($sentence, $conj) {
  
if ($sentence//word[@form=$conj][@relation="COORD"])

then local:modify-coord(  
 
 element sentence {$sentence/@*,
 let $focus := ($sentence//word[@form = $conj][@relation="COORD"])[1]
 for $w in $sentence//word
 return
 if (not($w/@id = $focus/@id)) 
 then 
 element word {attribute id {if (xs:integer($w/@id) > xs:integer($focus/@id)) 
                             then $w/@id + 1 else $w/@id
                            }, $w/@form , $w/@lemma, $w/@postag, $w/@relation,
                               $w/@artificial,
              attribute head {
              if (xs:integer($w/@head) >= xs:integer($focus/@id)) 
                             then $w/@head + 1 else $w/@head}
              }
 else
 (
  <word id="{$w/@id}" form="{substring($conj, 1, 2)}" 
        lemma="{substring($conj, 1, 2)}" 
        postag="d--------" 
        relation="AuxY" head="{$w/@id + 1}"/>, $w/@artificial,
  <word id="{$w/@id + 1}" form="{substring($conj, 3, 4)}" 
        lemma="{substring($conj, 3, 4)}" postag="c--------" 
        relation="{$w/@relation}" head="{
        if (xs:integer($w/@head) > xs:integer($focus/@id)) 
                             then $w/@head + 1 else $w/@head  
          
        }"/>, $w/@artificial
 )   
 }
,
$conj)

else $sentence 
 
};

declare function local:modify-auxy($sentence, $conj) {

let $cond := $sentence//word[@form=$conj][@relation="AuxY"]
             [@head = $sentence//word[@relation="COORD"]/@id]

return

if ($cond)

then local:modify-auxy(  
 
 element sentence {$sentence/@*,
 let $focus := ($cond)[1]
 for $w in $sentence//word
 return
 if (not($w/@id = $focus/@id)) 
 then 
 element word {attribute id {if (xs:integer($w/@id) > xs:integer($focus/@id)) 
                             then $w/@id + 1 else $w/@id
                            }, $w/@form , $w/@lemma, $w/@postag, $w/@relation,
                               $w/@artificial,
              attribute head {
              if (xs:integer($w/@head) >= xs:integer($focus/@id)) 
                             then $w/@head + 1 else $w/@head}
              }
 else
 (
  <word id="{$w/@id}" form="{substring($conj, 1, 2)}" 
        lemma="{substring($conj, 1, 2)}" 
        postag="d--------" 
        relation="AuxY" head="{$w/@id + 1}"/>, $w/@artificial,
  <word id="{$w/@id + 1}" form="{substring($conj, 3, 4)}" 
        lemma="{substring($conj, 3, 4)}" postag="c--------" 
        relation="{$w/@relation}" head="{
        if (xs:integer($w/@head) > xs:integer($focus/@id)) 
                             then $w/@head + 1 else $w/@head  
        }"/>, $w/@artificial
 )   
 }
,
$conj)

else $sentence 
 
};

(: the following is for oude, mede :)
declare function local:modify-auxz($sentence, $conj) {

let $cond := $sentence//word[@form=$conj][@relation="AuxZ"]

return

if ($cond)

then local:modify-auxz(  
 
 element sentence {$sentence/@*,
 let $focus := ($cond)[1]
 for $w in $sentence//word
 return
 if (not($w/@id = $focus/@id)) 
 then 
 element word {attribute id {if (xs:integer($w/@id) > xs:integer($focus/@id)) 
                             then $w/@id + 1 else $w/@id
                            }, $w/@form , $w/@lemma, $w/@postag, $w/@relation,
                             $w/@artificial,
              attribute head {
              if (xs:integer($w/@head) > xs:integer($focus/@id)) 
                             then $w/@head + 1 else $w/@head}
              }
 else
 (
  <word id="{$w/@id}" form="{substring($conj, 1, 2)}" 
        lemma="{substring($conj, 1, 2)}" 
        postag="d--------" 
        relation="AuxZ" head="{
        if (xs:integer($w/@head) > xs:integer($focus/@id)) 
        then $w/@head + 1 else $w/@head  
        }"/>, $w/@artificial,  $w/@artificial,
  <word id="{$w/@id + 1}" form="{substring($conj, 3, 4)}" 
        lemma="{substring($conj, 3, 4)}" postag="c--------" 
        relation="AuxY" head="{$w/@id}"/>, $w/@artificial
 )   
 }
,
$conj)

else $sentence 
 
};

(: the following is for eite :)
declare function local:modify-auxc($sentence, $conj) {

let $cond := $sentence//word[@form=$conj][@relation="AuxC"]

return

if ($cond)

then local:modify-auxc(  
 
 element sentence {$sentence/@*,
 let $focus := ($cond)[1]
 for $w in $sentence//word
 return
 if (not($w/@id = $focus/@id)) 
 then 
 element word {attribute id {if (xs:integer($w/@id) > xs:integer($focus/@id)) 
                             then $w/@id + 1 else $w/@id
                            }, $w/@form , $w/@lemma, $w/@postag, $w/@relation,
                              $w/@artificial,
              attribute head {
              if (xs:integer($w/@head) > xs:integer($focus/@id)) 
                             then $w/@head + 1 else $w/@head}
              }
 else
 (
  <word id="{$w/@id}" form="{substring($conj, 1, 2)}" 
        lemma="{substring($conj, 1, 2)}" 
        postag="c--------" 
        relation="AuxC" head="{
        if (xs:integer($w/@head) > xs:integer($focus/@id)) 
        then $w/@head + 1 else $w/@head  
        }"/>, $w/@artificial,
  <word id="{$w/@id + 1}" form="{substring($conj, 3, 4)}" 
        lemma="{substring($conj, 3, 4)}" postag="c--------" 
        relation="AuxY" head="{$w/@id}"/>,  $w/@artificial
 )   
 }
,
$conj)

else $sentence 
 
};

<treebank>{

let $sentences := local:normalize(
  
  local:add-artificial(       collection("/home/technician/Documents/" ||
         "transformer_parser_for_ancient_greek/original_texts/merge")
                      )         
                                 )
return

 for $s in $sentences
(: where $s[.//word[@form=("εἴτε", "οὐδὲ", "οὐδʼ", "εἴτʼ", "μηδʼ", "μηδὲ" )]] :)
 return
 local:modify-coord($s, "εἴτε") => local:modify-auxy("εἴτε") => 
 local:modify-auxc("εἴτε") =>
 
 local:modify-coord("εἴτʼ") => local:modify-auxy("εἴτʼ") => 
 local:modify-auxc("εἴτʼ") =>
 
 local:modify-coord("εἴθʼ") => local:modify-auxy("εἴθʼ") => 
 local:modify-auxc("εἴθʼ") =>
 
 
 local:modify-coord("οὔτε") => local:modify-auxy("οὔτε") => 
 local:modify-auxz("οὔτε") =>
 
 local:modify-coord("οὔτʼ") => local:modify-auxy("οὔτʼ") => 
 local:modify-auxz("οὔτʼ") =>
 
 local:modify-coord("οὔθʼ") => local:modify-auxy("οὔθʼ") => 
 local:modify-auxz("οὔθʼ") =>
 
 
 local:modify-coord("μήτε") => local:modify-auxy("μήτε") => 
 local:modify-auxz("μήτε") =>
 
 local:modify-coord("μήτʼ") => local:modify-auxy("μήτʼ") => 
 local:modify-auxz("μήτʼ") =>
 
 local:modify-coord("μήθʼ") => local:modify-auxy("μήθʼ") => 
 local:modify-auxz("μήθʼ") =>
 
 
 local:modify-coord("οὐδὲ") => local:modify-auxy("οὐδὲ") => 
 local:modify-auxz("οὐδὲ") =>
 
 local:modify-coord("οὐδʼ") => local:modify-auxy("οὐδʼ") => 
 local:modify-auxz("οὐδʼ") =>
 
 
 local:modify-coord("μηδὲ") => local:modify-auxy("μηδὲ") => 
 local:modify-auxz("μηδὲ") =>
 
 local:modify-coord("μηδʼ") => local:modify-auxy("μηδʼ")  => 
 local:modify-auxz("μηδʼ")

}</treebank> 


 (:
 εἴτε|οὐδὲ"|οὐδʼ|εἴτʼ|μηδʼ|μηδὲ" 
 :)