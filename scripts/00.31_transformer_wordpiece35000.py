import csv
import numpy as np
import tensorflow as tf
import tensorflow_text as text
from tensorflow import keras
from tensorflow.keras import layers
import random
from transformers import AutoTokenizer, TFAutoModel
import matplotlib.pyplot as plt

# https://www.tensorflow.org/text/guide/subwords_tokenizer
# https://keras.io/examples/nlp/text_classification_with_transformer/
# https://www.tensorflow.org/text/tutorials/transformer#multi-head_attention
# https://huggingface.co/bert-base-multilingual-cased

#np.random.seed(123)
#random.seed(123)
#tf.random.set_seed(1234)

train_p = "../normalized_texts/train.csv"
val_p = "../normalized_texts/val.csv"
test_p = "../normalized_texts/val.csv"

with open(train_p) as f:
   reader = csv.DictReader(f)
   train_data = [row for row in reader]

x_train = [v for x in train_data for k,v in x.items() 
                                                           if k == "sentence"]
y_train = [int(v) for x in train_data for k,v in x.items() 
                                                    if k == "elliptical_nodes"]
y_train = np.array(y_train)

with open(val_p) as f:
   reader = csv.DictReader(f)
   val_data = [row for row in reader]

x_val = [v for x in val_data for k,v in x.items() if
                                                              k == "sentence"]
y_val = [int(v) for x in val_data for k,v in x.items() if
                                                       k == "elliptical_nodes"]
y_val = np.array(y_val)

with open(test_p) as f:
   reader = csv.DictReader(f)
   test_data = [row for row in reader]

x_test = [v for x in test_data for k,v in x.items() if
                                                              k == "sentence"]
y_test = [int(v) for x in test_data for k,v in x.items() if
                                                       k == "elliptical_nodes"]
y_test = np.array(y_test)

# it turns out that it is reasonable to consider only the following
# number of elliptical nodes, the others being just a few examples
all_labels = ["[0]", "[1]", "[2]", "none"]

dict_y = dict(zip(all_labels, range(0, len(all_labels))))
dict_y_inv = {v: k for k, v in dict_y.items()}

bert_tokenizer_params=dict(lower_case=True)

gr_tokenizer = text.BertTokenizer('../normalized_texts/greek_vocab_35000.txt', 
                                  **bert_tokenizer_params)

matrix_x = gr_tokenizer.tokenize(x_train)
matrix_x = matrix_x.merge_dims(-2,-1)
lm = max([len(x) for x in matrix_x.to_list()])

with open('../normalized_texts/greek_vocab_35000.txt', 'r') as f:
    greek_vocab = f.read().splitlines()

def prepare_dataset(x, y, lm):
   tokenized = gr_tokenizer.tokenize(x)
   tokenized = tokenized.merge_dims(-2,-1)
   matrix_x = tf.keras.utils.pad_sequences(tokenized.numpy(), 
                     padding="post", maxlen=lm)
   return matrix_x, y

x_train, y_train = prepare_dataset(x_train, y_train, lm)
x_val, y_val = prepare_dataset(x_val, y_val, lm)
x_test, y_test = prepare_dataset(x_test, y_test, lm)

# model
class TransformerBlock(layers.Layer):
    def __init__(self,embed_dim, num_heads, ff_dim, rate=0.2,**kwargs):
        super().__init__(**kwargs)
        self.att = layers.MultiHeadAttention(num_heads=num_heads,
                                             key_dim=embed_dim)
        self.ffn = keras.Sequential([layers.Dense(ff_dim, activation="relu"), 
                                     layers.Dense(embed_dim),])
        self.layernorm1 = layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = layers.LayerNormalization(epsilon=1e-6)
        self.dropout1 = layers.Dropout(rate)
        self.dropout2 = layers.Dropout(rate)
        self.supports_masking = True

    def call(self, inputs, training, mask=None):
        #if mask is not None:
        mask_ = tf.cast(mask[:, tf.newaxis, :], dtype="int32")
        attn_output = self.att(inputs, inputs, attention_mask=mask_)
        attn_output = self.dropout1(attn_output, training=training)
        out1 = self.layernorm1(inputs + attn_output)
        ffn_output = self.ffn(out1)
        ffn_output = self.dropout2(ffn_output, training=training)
        return self.layernorm2(out1 + ffn_output), mask

class TokenAndPositionEmbedding(layers.Layer):
    def __init__(self, maxlen, vocab_size, embed_dim):
        super().__init__()
        self.token_emb = layers.Embedding(input_dim=vocab_size,
                                          output_dim=embed_dim, mask_zero=True)
        self.pos_emb = layers.Embedding(input_dim=maxlen,
                                        output_dim=embed_dim, mask_zero=True)

    def call(self, x):
        mask = self.token_emb.compute_mask(x)
        maxlen = tf.shape(x)[-1]
        positions = tf.range(start=0, limit=maxlen, delta=1)
        positions = self.pos_emb(positions)
        x = self.token_emb(x)
        return x + positions, mask

maxlen = lm
vocab_size = len(greek_vocab)
embed_dim = 2000  # Embedding size for each token
num_heads = 2  # Number of attention heads
ff_dim = 2000 # Hidden layer size in feed forward network inside transformer

#class_weights = compute_class_weight(class_weight = "balanced",
#                classes= np.unique(train_y), y= train_y)
#class_weights = dict(zip(range(0,len(dict_y)), class_weights))

inputs = layers.Input(shape=x_train.shape[1])
embedding_layer = TokenAndPositionEmbedding(maxlen, vocab_size, embed_dim)
x = embedding_layer(inputs)
transformer_block = TransformerBlock(embed_dim, num_heads, ff_dim)
x = transformer_block(x[0], mask=x[1])
x = layers.GlobalAveragePooling1D()(x[0], mask=x[1])
x = layers.Dropout(0.2)(x)
x = layers.Dense(500, activation="relu")(x)
x = layers.Dense(100, activation="relu")(x)
x = layers.Dropout(0.2)(x)
outputs = layers.Dense(len(dict_y), activation="softmax")(x)

model = keras.Model(inputs=inputs, outputs=outputs)
model.summary()
early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss',
                                                  patience=2,
                                                  restore_best_weights=True)
opt = tf.keras.optimizers.Adam(learning_rate=0.000001)
model.compile(optimizer=opt, loss="sparse_categorical_crossentropy",
              metrics=["sparse_categorical_accuracy"])
history = model.fit(
    x_train, y_train, epochs=30, batch_size=8, validation_data=[x_val, y_val], 
callbacks=[early_stopping]
#,class_weight=class_weights)
)


# model.save('../models/transformer_wordpiece35000/mb')
# model3 = keras.models.load_model('../models/transformer_wordpiece35000/mb')

model.evaluate(np.array(x_test), y_test)
#[0.5974756479263306, 0.7784430980682373]
pre = model.predict(np.array(x_test))
pre = np.argmax(pre, axis=-1)

# np.save('../models/transformer_wordpiece35000/predicted_test.npy', pre)
# pre= np.load('../models/transformer_wordpiece35000/predicted_test.npy',
#allow_pickle=True)

from sklearn.metrics import precision_recall_fscore_support
print(precision_recall_fscore_support(y_test, pre, average='micro', labels=[0,1,2,3]))
# (0.7784431137724551, 0.7784431137724551, 0.7784431137724551, None)
print(precision_recall_fscore_support(y_test, pre, average='macro', labels=[0,1,2,3]))
# (0.44488345333125245, 0.2849744968388501, 0.2851597698633701, None)
print(precision_recall_fscore_support(y_test, pre, average='weighted', labels=[0,1,2,3]))
# (0.7068300748911358, 0.7784431137724551, 0.7129790168864409, None)
print(precision_recall_fscore_support(y_test, pre, average=None, labels=[0,1,2,3]))
#(array([0.48630137, 0.        , 0.5       , 0.79323244]),
# array([0.12655971, 0.        , 0.03370787, 0.97963041]),
# array([0.20084866, 0.        , 0.06315789, 0.87663253]),
# array([1122,  206,   89, 4762]))

from sklearn.metrics import confusion_matrix
print(confusion_matrix(y_test, pre))
#array([[ 142,    0,    0,  980],
#       [  33,    0,    0,  173],
#       [  23,    0,    3,   63],
#       [  94,    0,    3, 4665]])

# np.save('../models/transformer_wordpiece35000/my_history.npy',history.history)
# history=np.load('../models/transformer_wordpiece35000/my_history.npy',allow_pickle='TRUE').item()

'''
Epoch 1/30
5407/5407 [==============================] - 472s 86ms/step - loss: 0.6806 - sparse_categorical_accuracy: 0.7679 - val_loss: 0.6501 - val_sparse_categorical_accuracy: 0.7716
Epoch 2/30
5407/5407 [==============================] - 429s 79ms/step - loss: 0.6511 - sparse_categorical_accuracy: 0.7728 - val_loss: 0.6346 - val_sparse_categorical_accuracy: 0.7725
Epoch 3/30
5407/5407 [==============================] - 428s 79ms/step - loss: 0.6357 - sparse_categorical_accuracy: 0.7739 - val_loss: 0.6246 - val_sparse_categorical_accuracy: 0.7737
Epoch 4/30
5407/5407 [==============================] - 426s 79ms/step - loss: 0.6219 - sparse_categorical_accuracy: 0.7747 - val_loss: 0.6185 - val_sparse_categorical_accuracy: 0.7747
Epoch 5/30
5407/5407 [==============================] - 426s 79ms/step - loss: 0.6075 - sparse_categorical_accuracy: 0.7762 - val_loss: 0.6076 - val_sparse_categorical_accuracy: 0.7755
Epoch 6/30
5407/5407 [==============================] - 425s 79ms/step - loss: 0.5911 - sparse_categorical_accuracy: 0.7781 - val_loss: 0.5992 - val_sparse_categorical_accuracy: 0.7760
Epoch 7/30
5407/5407 [==============================] - 425s 79ms/step - loss: 0.5746 - sparse_categorical_accuracy: 0.7815 - val_loss: 0.6007 - val_sparse_categorical_accuracy: 0.7762
Epoch 8/30
5407/5407 [==============================] - 425s 79ms/step - loss: 0.5586 - sparse_categorical_accuracy: 0.7865 - val_loss: 0.5975 - val_sparse_categorical_accuracy: 0.7784
Epoch 9/30
5407/5407 [==============================] - 425s 79ms/step - loss: 0.5440 - sparse_categorical_accuracy: 0.7898 - val_loss: 0.6023 - val_sparse_categorical_accuracy: 0.7784
Epoch 10/30
5407/5407 [==============================] - 424s 78ms/step - loss: 0.5282 - sparse_categorical_accuracy: 0.7940 - val_loss: 0.6126 - val_sparse_categorical_accuracy: 0.7802

'''
