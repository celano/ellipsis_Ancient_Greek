import json
import numpy as np
import tensorflow as tf
import tensorflow_text as text
from tensorflow import keras
from tensorflow.keras import layers
from sklearn.utils.class_weight import compute_class_weight
from sklearn.model_selection import train_test_split
import random
from transformers import AutoTokenizer, TFAutoModel
import matplotlib.pyplot as plt

##### IMPORT DATASET

path = "../normalized_texts/sentences_for_machine_learning.json"
with open(path, 'r') as f:
    data = json.load(f)

# transform all targets into a list ['[0]', '[1]']
data = [dict(x, target=x["target"].split()) for x in data]

##### PREPARE Y

# ellipsis values are ordered, so that '[1]' implies the existence of '[0]'
values_ellipsis = [x["target"][-1] for x in data]
all_labels = set(values_ellipsis)
dict_plot = {x: values_ellipsis.count(x) for x in all_labels}


### PLOT
#import matplotlib.style as style
#style.use("seaborn-v0_8-dark")
def create_plot():
    dict_plot2 = {int(k.replace("[", "").replace("]", "")) + 1:v for k,v
    in dict_plot.items() if k != "none"}
    dict_plot2 = sorted(dict_plot2.items())
    XS = [str(x[0]) for x in dict_plot2]
    XS.insert(0, "none")
    YS = [x[1] for x in dict_plot2]
    YS.insert(0, dict_plot["none"])
    sall = sum(YS)
    YS2 = list(np.round(np.array(YS)/sall, 3))
    fig, (ax1, ax2) = plt.subplots(2,1, sharex=True)
    fig.set_size_inches(10, 10)
    #fig.set_dpi(100)
    gradient = plt.cm.gray(np.linspace(0.6, 0.9, len(YS)))
    bar_container = ax1.bar(XS, YS2, zorder=3, color=
    ["#115f9a", "#1984c5", "#22a7f0", "#48b5c4", "#76c68f",
    "#a6d75b", "#c9e52f", "#d0ee11", "#d0f400"]
     )
    ax1.bar_label(bar_container, labels=YS)
    ax1.set(ylabel='frequency')
    #ax2.plot(XS, np.cumsum(YS2), color="#115f9a")
    #ax2.plot(["3", "3", "3", "3"], np.cumsum(YS2)[:4], color="red")
    ax2.plot(XS[:4], np.cumsum(YS2)[:4], color="#115f9a")
    ax2.plot(XS[3:],np.cumsum(YS2)[3:], color="red")
    ax2.text(XS[3], np.cumsum(YS2)[3]-0.01, '({}, {})'.format(XS[3], "%.3f" % np.cumsum(YS2)[3]))
    ax2.set_xlabel("number of elliptical nodes per sentence")
    ax2.set_ylabel("cumulative frequency")
    ax1.tick_params(top=True, labeltop=True, bottom=False, labelbottom=False)
    #ax2.set_title("cumulative frequency for elliptical nodes",fontweight="bold")
    #ax1.set_title("number of elliptical nodes per sentence")
    #plt.savefig("../article/colored_bar.svg", format="svg")
    ax1.xaxis.set_label_position('top')
    ax1.set_xlabel("number of elliptical nodes per sentence")
    ax1.grid(zorder=0)
    ax2.grid()
    #, dpi=resolution_value)
    plt.show()

create_plot()

