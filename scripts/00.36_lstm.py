import csv
import numpy as np
import tensorflow as tf
import tensorflow_text as text
from tensorflow import keras
from tensorflow.keras import layers
import random
from transformers import AutoTokenizer, TFAutoModel
import matplotlib.pyplot as plt

train_p = "../normalized_texts/train.csv"
val_p = "../normalized_texts/val.csv"
test_p = "../normalized_texts/val.csv"

with open(train_p) as f:
   reader = csv.DictReader(f)
   train_data = [row for row in reader]

x_train = [v for x in train_data for k,v in x.items() 
                                                           if k == "sentence"]
y_train = [int(v) for x in train_data for k,v in x.items() 
                                                    if k == "elliptical_nodes"]
y_train = np.array(y_train)

with open(val_p) as f:
   reader = csv.DictReader(f)
   val_data = [row for row in reader]

x_val = [v for x in val_data for k,v in x.items() if
                                                              k == "sentence"]
y_val = [int(v) for x in val_data for k,v in x.items() if
                                                       k == "elliptical_nodes"]
y_val = np.array(y_val)

with open(test_p) as f:
   reader = csv.DictReader(f)
   test_data = [row for row in reader]

x_test = [v for x in test_data for k,v in x.items() if
                                                              k == "sentence"]
y_test = [int(v) for x in test_data for k,v in x.items() if
                                                       k == "elliptical_nodes"]
y_test = np.array(y_test)


all_labels = ["[0]", "[1]", "[2]", "none"]
dict_y = dict(zip(all_labels, range(0, len(all_labels))))
dict_y_inv = {v: k for k, v in dict_y.items()}

# https://huggingface.co/pranaydeeps/Ancient-Greek-BERT/blob/main/
# pytorch_model.bin
# 24 Sept 2021
tokenizer = AutoTokenizer.from_pretrained("pranaydeeps/Ancient-Greek-BERT")
model_pre = TFAutoModel.from_pretrained("pranaydeeps/Ancient-Greek-BERT", 
                                        from_pt=True)

# the following is necessary to know the longest sentence length in train:
# if a longer sentence is in valid or test, it is truncated
tokenized = tokenizer(x_train, return_tensors="tf", padding="longest")
max_length = tokenized["input_ids"].shape[1] # 364
print(max_length)
del tokenized

def create_dataset(x, y):
  tokenized = tokenizer(x, return_tensors="tf", max_length=max_length,
                                                padding="max_length")
  x = tf.data.Dataset.from_tensor_slices((tokenized["input_ids"],
                                          tokenized["attention_mask"]))
  y = tf.data.Dataset.from_tensor_slices(y)
  xy = tf.data.Dataset.zip((x,y))
  return xy.batch(8)

train = create_dataset(x_train, y_train)
val = create_dataset(x_val, y_val)
test = create_dataset(x_test, y_test)




##### MODEL

# class weights do not seem to improve results, i.e, both loss and accuracy
# class_weights = compute_class_weight(class_weight = "balanced",
#                 classes= np.unique(matrix_y), y= matrix_y)
# class_weights = dict(zip(range(0,len(dict_y)), class_weights))

inputs = layers.Input(shape=(max_length,), dtype=tf.int32)
masks = layers.Input(shape=(max_length,), dtype=tf.int32)
x = model_pre.bert(inputs, attention_mask=masks)["last_hidden_state"]
x = layers.LSTM(1000) (x, mask=tf.cast(masks, tf.bool))
x = layers.Dropout(0.2)(x)
x = layers.Dense(500, activation="relu")(x)
x = layers.Dense(100, activation="relu")(x)
x = layers.Dropout(0.2)(x)
outputs = layers.Dense(len(dict_y), activation="softmax")(x)

model = keras.Model(inputs=[inputs, masks], outputs=outputs)
model.summary()

#path_save = "../models/tr035/tr035polished"
early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss',
                                                  patience=2,
                                                  restore_best_weights=True)
#best_model = tf.keras.callbacks.ModelCheckpoint(
#    filepath=path_save,
#    monitor="val_loss",
#    save_weights_only=True,
#    mode='min',
#    save_best_only=True)

opt = tf.keras.optimizers.Adam(learning_rate=0.000001)
model.compile(optimizer=opt, loss="sparse_categorical_crossentropy",
              metrics=["sparse_categorical_accuracy"])
history = model.fit(train, epochs=10, validation_data=[val]
,callbacks=[early_stopping]
#,class_weight=class_weights
                   )

##### EVALUATE

model.evaluate(test)
#[0.4234582483768463, 0.8467389345169067]
pre = model.predict(test)
pre = np.argmax(pre, axis=-1)

# np.save('../models/lstm_corrected/predicted_test.npy', pre)

from sklearn.metrics import precision_recall_fscore_support
print(precision_recall_fscore_support(y_test, pre, average='micro', labels=[0,1,2,3]))
# (0.8467389545233857, 0.8467389545233857, 0.8467389545233857, None)
print(precision_recall_fscore_support(y_test, pre, average='macro', labels=[0,1,2,3]))
#  (0.6094961727038761, 0.4861033524142491, 0.527781399618702, None)
print(precision_recall_fscore_support(y_test, pre, average='weighted', labels=[0,1,2,3]))
# (0.8277851955353673, 0.8467389545233857, 0.8334046743840481, None)
print(precision_recall_fscore_support(y_test, pre, average=None, labels=[0,1,2,3]))
#(array([0.65393013, 0.43298969, 0.45833333, 0.89273154]),
# array([0.53386809, 0.2038835 , 0.24719101, 0.95947081]),
# array([0.58783121, 0.27722772, 0.32116788, 0.92489879]),
# array([1122,  206,   89, 4762]))


from sklearn.metrics import confusion_matrix
print(confusion_matrix(y_test, pre))
#array([[ 599,   29,   10,  484],
#       [  97,   42,   15,   52],
#       [  30,   24,   22,   13],
#       [ 190,    2,    1, 4569]])


# np.save('../models/lstm_corrected/my_history.npy',history.history)
# history=np.load('../models/lstm_corrected/my_history.npy',allow_pickle='TRUE').item()

# model.save('../models/lstm_corrected/mb')
# model3 = keras.models.load_model('../models/lstm_corrected/mb')

# model.save_weights("../models/lstm_corrected/weights")
# model.load_weights("../models/lstm_corrected/weights")

