# Normalized texts

The texts of the Ancient Greek Treebank 2.1 and the Gorman Trees
(see ../texts/README.md for more information) are made available
for machine learning processing in the file
"sentences_for_machine_learning.json"
(or in the original xml format of the sentences, "all_sentences.xml"),
where the original texts (see the directory original_texts)
have been normalized thus:

* The greek has been NFC-normalized and the apostrophe-looking characters have
been normalized as Modifier Letter Apostrophe (U+02BC).
* The particles "εἴτε", "εἴτʼ","εἴθʼ","μήτε", "μήτʼ","μήθʼ",
οὐδὲ", "οὐδʼ", "μηδʼ", and "μηδὲ" have been tokenized to meet the tokenization
scheme of *Opera Graeca Adnotata*.

The folder also provides the same sentences divided into train (~80%),
dev (~10%), and test (10%) sets (stratified for the y classes).

The folder also has a vocabulary created with a WordPiece algorithm 
"greek_vocab_35000.txt".

