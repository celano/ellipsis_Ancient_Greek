## A Neural Network Approach to Ellipsis Detection in Ancient Greek

This repository contains data concerning the article:

Giuseppe G. A. Celano, *A Neural Network Approach to Ellipsis Detection in Ancient Greek*,
to be published in the proceedings of the 
6th International Conference on Natural Language (ICNLSP 2013)

The study presents a model that can ingest a tokenized Ancient Greek sentence
and predict how many elliptical nodes it contains.

In the repository, one can find:

* A model to predict ellipsis in Ancient Greek in ``/model/feedforward_bert_we``, 
which I call Elleipsis
* A normalized version of the treebank data in ``/normalized_texts``

In order to use the model, download the repository, go to ``/scripts``, and type:

``python 00.39_annotate_ellipsis.py path_to_file_to_parse path_where_to_save_results``

To see what the input and the output of the model look like,
see the example files, respectively:
* ``../models/feedforward_bert_we/sentences_to_parse.txt``
* ``../models/feedforward_bert_we/sentences_parsed.txt``

To test the model, one can use the provided file ``sentences_to_parse.txt``: 

``python 00.39_annotate_ellipsis.py ../models/feedforward_bert_we/sentences_to_parse.txt ../models/feedforward_bert_we/sentences_parsed.txt``

As can be seen in the example file 
``../models/feedforward_bert_we/sentences_parsed.txt``, the output of the
parser is an original Greek sentence followed by:
* ``[1]``, if one elliptical node is predicted
* ``[2]``, if two elliptical nodes are predicted
* ``[3]``, if three elliptical nodes are predicted
* ``[4]``, if the relevant sentence has no elliptical nodes
